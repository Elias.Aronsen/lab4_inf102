package INF102.lab4.median;

import java.util.ArrayList;
import java.util.List;

public class QuickMedian implements IMedian {

    @Override
    public <T extends Comparable<T>> T median(List<T> list) {
        List<T> copy = new ArrayList<>(list); // Method should not alter list

        return findMedian(copy, copy.size() / 2);
    }

    public <T extends Comparable<T>> T findMedian(List<T> list, int x) {
        T result;
        T pivot = list.get(x / 2);
        List<T> smaller = new ArrayList<T>();
        List<T> larger = new ArrayList<T>();
        List<T> equal = new ArrayList<T>();
        for (T t : list) {
            if (t.compareTo(pivot) > 0) {
                larger.add(t);
            } else if (t.compareTo(pivot) < 0) {
                smaller.add(t);
            } else {
                equal.add(t);
            }
        }

        if (x < smaller.size()) {
            result = findMedian(smaller, x);
        } else if (x < smaller.size() + equal.size()) {
            result = pivot;
        } else {
            result = findMedian(larger, x - smaller.size() - equal.size());
        }
        return result;

    }

}
